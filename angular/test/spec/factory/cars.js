'use strict';

describe('Service: ../factory/cars', function () {

  // load the service's module
  beforeEach(module('angularApp'));

  // instantiate service
  var ../factory/cars;
  beforeEach(inject(function (_../factory/cars_) {
    ../factory/cars = _../factory/cars_;
  }));

  it('should do something', function () {
    expect(!!../factory/cars).toBe(true);
  });

});
