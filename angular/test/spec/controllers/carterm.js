'use strict';

describe('Controller: CartermCtrl', function () {

  // load the controller's module
  beforeEach(module('angularApp'));

  var CartermCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CartermCtrl = $controller('CartermCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CartermCtrl.awesomeThings.length).toBe(3);
  });
});
