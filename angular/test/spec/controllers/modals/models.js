'use strict';

describe('Controller: ModalsModelsCtrl', function () {

  // load the controller's module
  beforeEach(module('angularApp'));

  var ModalsModelsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsModelsCtrl = $controller('ModalsModelsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ModalsModelsCtrl.awesomeThings.length).toBe(3);
  });
});
