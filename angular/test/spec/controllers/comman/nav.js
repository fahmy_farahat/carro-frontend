'use strict';

describe('Controller: CommanNavCtrl', function () {

  // load the controller's module
  beforeEach(module('angularApp'));

  var CommanNavCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CommanNavCtrl = $controller('CommanNavCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CommanNavCtrl.awesomeThings.length).toBe(3);
  });
});
