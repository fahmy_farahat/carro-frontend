'use strict';

describe('Controller: CommanFooterCtrl', function () {

  // load the controller's module
  beforeEach(module('angularApp'));

  var CommanFooterCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CommanFooterCtrl = $controller('CommanFooterCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CommanFooterCtrl.awesomeThings.length).toBe(3);
  });
});
