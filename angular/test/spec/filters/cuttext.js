'use strict';

describe('Filter: cutText', function () {

  // load the filter's module
  beforeEach(module('angularApp'));

  // initialize a new instance of the filter before each test
  var cutText;
  beforeEach(inject(function ($filter) {
    cutText = $filter('cutText');
  }));

  it('should return the input prefixed with "cutText filter:"', function () {
    var text = 'angularjs';
    expect(cutText(text)).toBe('cutText filter: ' + text);
  });

});
