# angular

##Installation

Provided that Nodejs is already installed

npm install -g yo grunt-cli bower

npm install
bower install

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
