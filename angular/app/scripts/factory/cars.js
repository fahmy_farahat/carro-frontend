'use strict';

/**
 * @ngdoc service
 * @name angularApp cars
 * @description
 * Factory in the angularApp.
 */
angular.module('angularApp')
.factory("$cookieStore", ["$cookies", function(cookies) {
    return {
        get: function(k) {
            return cookies.getObject(k);
        },
        put: function(k, v) {
            cookies.putObject(k, v);
        },
        remove: function(k) {
            cookies.remove(k);
        }
    };
}]);