'use strict';

/**
 * @ngdoc overview
 * @name angularApp
 * @description
 * # angularApp
 *
 * Main module of the application.
 */
var modules = [
    'ngResource',
    'ngRoute',
    'ngCookies',
    'angular-loading-bar',

    'infinite-scroll',
    'ui.bootstrap'
  ];

var angularApp = angular.module('angularApp', modules);

  angularApp.constant('URLS', {
    "getCars":"http://www.trustycars.com.sg/listings.json",
    'getPopularModel':'http://carro.sg/getPopularModel',
    'getdata':'http://carro.sg/getdata'
  });
angularApp.config(['$routeProvider', 'cfpLoadingBarProvider', 
    function($routeProvider, cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeBar = true;
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/car/:makeid/:modelid', {
        templateUrl: 'views/carterm.html',
        controller: 'CartermCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
}]);
