'use strict';

/**
 * @ngdoc service
 * @name angularApp.utils
 * @description
 * # utils
 * Service in the angularApp.
 */
(function(){
    function Utils (rootScope){

        this._validate = function(a, b) {
            return null === a || null === b || "string" == typeof b ? !0 : b > a
        };

        this._isLightMin = function(b, min, max) {
            return b < min ? "inactive" : 
            null !== max && b >= max ? "disabled" :
            b === min || 0 === b && null === min ? "min-price" : ""
        };

        this._isLightMax = function(b, min, max) {
            return null !== max && ("string" == typeof b || b > max) ? "inactive" :
            null !== min && b <= min ? "disabled" :
            b === max || 200000 === b && null === max ? "max-price" : ""
        };

        this._setFilters = function(open){
            if (!open) {
                rootScope.$broadcast('getCars');
            }
        };
    }
    angular.module('angularApp')
        .service('Utils', ['$rootScope', Utils])
}).call(null);