'use strict';

/**
 * @ngdoc service
 * @name angularApp.Cars
 * @description
 * # Cars
 * Service in the angularApp.
 */
(function() {
    function Cars(http, URLS) {
        var service = {};

        service.listings = function(data){
            return http.get(URLS.getCars, {params: data});
        };

        service.listingsModel = function(urls, data){
            return http.get(
                URLS.getCars + '?makeid=' + urls.makeid + '&modelid=' + urls.modelid,
            {
                params: data
            });
        };

        service.getPopularModel = function(data){
            return http.get(URLS.getPopularModel, {params: data});
        };
                
        service.getData = function(){
            return http.get(URLS.getdata);
        };

        service.getDataTerm = function(term){
            return http.get(URLS.getdata + '?term=' + term);
        };

        return service;
    }

    angular.module("angularApp")
        .service("Cars", ['$http', 'URLS', Cars]);
}).call(null);