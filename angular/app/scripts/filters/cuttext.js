'use strict';

/**
 * @ngdoc filter
 * @name angularApp.filter:cutText
 * @function
 * @description
 * # cutText
 * Filter in the angularApp.
 */
angular.module('angularApp')
.filter('truncate', function (){
  return function (text, length, end){
    if (text !== undefined){
      if (isNaN(length)){
        length = 10;
      }

      end = end || "...";

      if (text.length <= length || text.length - end.length <= length){
        return text;
      }else{
        return String(text).substring(0, length - end.length) + end;
      }
    }
  };
});
/**
 * @ngdoc filter
 * @name angularApp.filter:currencyLen
 * @function
 * @description
 * # currencyLen
 * Filter in the angularApp.
 */
angular.module('angularApp')
    .filter('currencyLen', function (){
        return function (input){
            if (input === 200000) {
                return '$' + input / 1000 + '+ K';
            }else{
                return '$' + input / 1000 + 'K';
            }
        };
});
/**
 * @ngdoc filter
 * @name angularApp.filter:currencyLen
 * @function
 * @description
 * # currencyLen
 * Filter in the angularApp.
 */
angular.module('angularApp')
    .filter('mileLen', function (){
        return function (input){
            if (input === 300000) {
                return input / 1000 + '+ K';
            }else{
                return input / 1000 + 'K';
            }
        };
});