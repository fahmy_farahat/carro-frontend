'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:CartermCtrl
 * @description
 * # CartermCtrl
 * Controller of the angularApp
 */
(function(){
    function controller (scope, Cars, params){

        scope.cars = [];
        scope.busy = false;

        scope.loadMore = function(){
            if(scope.busy) {return;}
            scope.busy = true;
            filter.page +=1;
            _getCars();
        };

        var filter = {
            'page': 0,
            'per_page': 6
        };
        
        var _getCars = function(){
            // console.log(filter);
            Cars.listingsModel(params, filter).then(function(resposnse){
                var cars = resposnse.data;
                angular.forEach(cars.data, function(car){
                    scope.cars.push(car);
                    scope.busy = false;
                });
            }, function(error){
                console.log(error);
            });
        };
        _getCars();

    }
    angular.module('angularApp')
      .controller('CartermCtrl', ['$scope', 'Cars', '$routeParams', controller]);
}).call(null);