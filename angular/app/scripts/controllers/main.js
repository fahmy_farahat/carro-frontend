'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularApp
 */
(function(){
    function controller (scope, Cars, cookies){
        scope.cars = [];
        scope.busy = false;
        scope.filter = {
            'page':0,
            'per_page':6,
        };

        scope.loadMore = function(){
            if(scope.busy) {return;}
            scope.busy = true;
            scope.filter.page +=1;
            _getCars();
        };
        var _getCars = function(){
            var filter = cookies.getObject('filter') || {};
            filter['page'] = scope.filter['page'];
            filter['per_page'] = scope.filter['per_page'];
            // filter['transmission'] = ['manual'];
            console.log(filter);
            Cars.listings(filter).then(function(resposnse){
                var cars = resposnse.data;
                angular.forEach(cars.data, function(car){
                    scope.cars.push(car);
                    scope.busy = false;
                });
            }, function(error){
                console.log(error);
            });
        };
        // _getCars();
        scope.$on('getCars', function(){
            scope.cars = [];
            _getCars();
        });

    }

    angular.module('angularApp')
  .controller('MainCtrl', ['$scope', 'Cars', '$cookies', controller]);
}).call(null);