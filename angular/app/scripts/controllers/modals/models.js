'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:ModalsModelsCtrl
 * @description
 * # ModalsModelsCtrl
 * Controller of the angularApp
 */
(function(){
    function controller (scope, Cars){

        scope.term = '';
        
        Cars.getData().then(function(res){
            scope.models = res.data;
        });

        //model search
        scope.$watch('term', function(){
            if(scope.term.length){
                Cars.getDataTerm(scope.term)
                .then(function(response){
                    scope.models = response.data;
                }, function(error){
                    console.log('search error');
                });
            }
        });

    }
    angular.module('angularApp')
      .controller('ModalsModelsCtrl', ['$scope', 'Cars', controller]); 
}).call(null);