'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:CommanNavCtrl
 * @description
 * # CommanNavCtrl
 * Controller of the angularApp
 */
(function(){
    function controller (scope, Cars, $modal, rootScope, cookies, Utils){

        var Utils = Utils;
        var cookiesFilter = cookies.getObject('filter') || {};
        console.log(cookiesFilter);
        var minRangeArr = [0,10000,20000,30000,40000,50000,70000,90000,110000,140000,170000];
        var maxRangeArr = [10000,20000,30000,40000,50000,60000,80000,100000,130000,160000,200000];
        scope.term = '';
        scope.filter = {
            'price_min':null || cookiesFilter.price_min || 0,
            'price_max':null || cookiesFilter.price_max || 200000,
            'depre_min':null || cookiesFilter.depre_min || 0,
            'depre_max':null || cookiesFilter.depre_max || 200000,
            'mile_min':null,
            'mile_max':null || cookiesFilter.mile_max || 300000 ,
            'year':2003,
            'body':null,
            'no_owner':null,
            'transmission':null
        };

        //PriceRange Filter
        scope.priceRange = {
            minPrices: minRangeArr,
            maxPrices: maxRangeArr,
            isLightMin: function(b){
                return Utils._isLightMin(b, scope.filter['price_min'], scope.filter['price_max']);
            },
            isLightMax: function(b){
                return Utils._isLightMax(b, scope.filter['price_min'], scope.filter['price_max']);
            },
            setMin: function(min){
                if(Utils._validate(min, scope.filter['price_max'])){
                    scope.filter['price_min'] = min;
                    return cookies.putObject('filter', scope.filter);
                }
            },   
            setMax: function(max){
                if(Utils._validate(scope.filter['price_min'], max)){
                    scope.filter['price_max'] = max;
                    return cookies.putObject('filter', scope.filter);
                }
            },
            setFilter: function(open) {
                Utils._setFilters(open);
            }
        };

        //Depre Range Filter.
        scope.depreRange = {
            depreMin: minRangeArr,
            depreMax: maxRangeArr,
            isLightMin: function(b){
                return Utils._isLightMin(b, scope.filter.depre_min, scope.filter.depre_max);
            },
            isLightMax: function(b){
                return Utils._isLightMax(b, scope.filter.depre_min, scope.filter.depre_max);
            },
            setMin: function(min){
                if (Utils._validate(min, scope.filter.depre_max)) {
                    scope.filter.depre_min = min;
                    return cookies.putObject('filter', scope.filter);
                }
            },
            setMax: function(max){
                if (Utils._validate(scope.filter.depre_min, max)) {
                    scope.filter.depre_max = max;
                    return cookies.putObject('filter', scope.filter);
                }
            },
            setFilter: function(open){
                Utils._setFilters(open);
            }
        };

        scope.mileageFilter = {
            mileages: [10000, 20000, 30000, 40000, 50000, 70000, 110000, 120000, 150000, 300000],
            setMileage: function(b) {
                scope.filter['mile_max'] = b;
                return cookies.putObject('filter', scope.filter);
            },
            isLight: function(b) {
                return null === scope.filter['mile_max'] ? "" : b > scope.filter['mile_max'] ? "inactive" : ""
            },
            prettify: function(a) {
                return a = a || 0, a.toLocaleString("hi-IN")
            },
            setFilter: function(open){
                Utils._setFilters(open);
            }
        };

        //Years filtering..
        scope.yearFilter = {
            years: _.range(2015, 2003, -1),
            setYear: function(b) {
                scope.filter.year = b;
                return cookies.putObject('filter', scope.filter);
            },
            isLight: function(b) {
                return null === scope.filter.year ? "" : b < scope.filter.year ? "inactive" : ""
            },
            setFilter: function(open){
                Utils._setFilters(open);
            }
        };

        //More Filters..
        scope.moreFilters = {
            transmission: {
                transmissions: ["Manual", "Automatic"],
                setTransmission: function(b) {
                    var c = scope.filter.transmission || [];
                    c.indexOf(b) > -1 ? c.splice(c.indexOf(b), 1) : c.push(b);
                    scope.filter.transmission = c;
                    console.log(scope.filter.transmission);
                    // a.filterMap.more.setCount(), 
                    return cookies.putObject('filter', scope.filter);
                }
            }
        };

        Cars.getPopularModel().then(function(response){
            scope.models = response.data;
        }, function(error){
            console.log(error);
        });

        scope.resetFilters = function(){
            cookies.remove('filter');
            scope.filter = {
                'price_min':0,
                'price_max':200000,
                'depre_min':0,
                'depre_max':200000,
                'mile_max':null,
                'year':2003,
                'mile_min':300000,
                'body':null,
                'no_owner':null,
                'transmission':null
            };
            Utils._setFilters();
        };

        //model search
        scope.$watch('term', function(){
            if(scope.term.length){
                Cars.getDataTerm(scope.term)
                .then(function(response){
                    scope.models = response.data;
                }, function(error){
                    console.log('search error');
                });
            }
        });

        //Open Models ADD/Remove
        scope.openModels = function(){
            var modalInstance = $modal.open({
              templateUrl: 'views/modals/models.html',
              controller: 'ModalsModelsCtrl',
              size: 'lg'
            });
        };

    }

    angular.module('angularApp')
        .controller('CommanNavCtrl', ['$scope', 'Cars', '$modal', '$rootScope', '$cookies', 'Utils', controller]);
  
}).call(null);